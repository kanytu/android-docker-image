#!/usr/bin/env bash

BOOT_COMMAND="adb shell getprop init.svc.bootanim"
READY_COMMAND="adb shell pm list packages"
OUT=`$BOOT_COMMAND`
RES="stopped"

while [[ ${OUT:0:7}  != 'stopped' ]]; do
		OUT=`$BOOT_COMMAND`
		echo 'Waiting for emulator to fully boot...'
		sleep 5
done

echo "Emulator booted!"

OUT=`$READY_COMMAND`
while [[ ${OUT:0:7}  != 'package' ]]; do
		OUT=`$READY_COMMAND`
		echo 'Waiting for Package Manager ...'
		sleep 5
done

echo "Emulator ready!"