FROM openjdk:8-jdk
MAINTAINER Pedro Oliveira <kanytu@gmail.com>

# Install Git and dependencies
RUN dpkg --add-architecture i386 \
 && apt-get update \
 && apt-get install -y file git curl zip libncurses5:i386 libstdc++6:i386 zlib1g:i386 \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists /var/cache/apt

# Set up environment variables
ENV ANDROID_HOME=$PWD/android-sdk-linux \
	ANDROID_SDK_ROOT=$PWD/android-sdk-linux \
    SDK_URL="https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip"

ENV PATH ${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/tools:${PATH}
 
# Download Android SDK
RUN mkdir "$ANDROID_HOME" .android \
 && cd "$ANDROID_HOME" \
 && curl -o sdk.zip $SDK_URL \
 && unzip sdk.zip \
 && rm sdk.zip \
 && yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses \
 && $ANDROID_HOME/tools/bin/sdkmanager "platform-tools" "emulator" "platforms;android-28" "build-tools;28.0.3" \ 
 && $ANDROID_HOME/tools/bin/sdkmanager --update \ 
 && $ANDROID_HOME/tools/bin/sdkmanager "system-images;android-21;google_apis;armeabi-v7a" \
 && echo no | $ANDROID_HOME/tools/bin/avdmanager create avd -n emuTest --package 'system-images;android-21;google_apis;armeabi-v7a'

 COPY wait-for-avd-boot.sh $ANDROID_HOME/wait-for-avd-boot.sh
 RUN chmod 777 $ANDROID_HOME/wait-for-avd-boot.sh